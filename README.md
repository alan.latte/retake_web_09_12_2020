# makarov05

## Project setup
```
npm install
```

### Json server start
```
json-server -w db/db.json -p 3456
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
